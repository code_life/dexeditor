## 概述
该项目主要用于在手机上对apk(.dex)进行修改,常用于反汇编apk实现破解APK。

另外该项目不是原创，主要是对原项目的一些修改及美化。由于原项目都是在手机上完成的，现在换成用IntelliJ IDEA完成，同时去掉了原有的Makefile等文件。

## 项目说明
1. 原项目地址：**[https://code.google.com/p/dex-editor/](https://code.google.com/p/dex-editor/)**
2. 调整代码结构，有强迫症，伤不起←_←

  * 删除一些无用代码import,如dalvik.system.VMRuntime
  * 添加InputConnection未实现方法，否则编译器报错
  * 整合字符常量池到菜单中
  * 文本编辑框默认不弹出输入法

## 特色
1. 添加minSdkVersion:10,targetSdkVersion:14,也就是Android 4.0以上手机默认是Holo主题(这也是我的初始意图 ←_← )